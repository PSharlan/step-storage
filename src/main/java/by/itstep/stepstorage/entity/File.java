package by.itstep.stepstorage.entity;

import javax.persistence.*;

import by.itstep.stepstorage.entity.enums.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "files")
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "link", unique = true)
    private String link;

    @Enumerated(STRING)
    @Column(name = "type")
    private Type type;

    @Enumerated(STRING)
    @Column(name = "service")
    private Service service;
}
