package by.itstep.stepstorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(StepStorageApplication.class, args);
    }

}
