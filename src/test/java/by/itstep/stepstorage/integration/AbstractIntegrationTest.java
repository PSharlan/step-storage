package by.itstep.stepstorage.integration;

import by.itstep.stepstorage.integration.initializer.MySqlInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@Testcontainers
@ContextConfiguration(initializers = MySqlInitializer.class)
public abstract class AbstractIntegrationTest {


}
