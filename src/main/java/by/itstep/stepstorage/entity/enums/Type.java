package by.itstep.stepstorage.entity.enums;

public enum Type {
    //Please keep an alphabetical order

    DOCUMENT,
    IMAGE,
    VIDEO,
    OTHER                  //Just in case if other file types don't suit

}
