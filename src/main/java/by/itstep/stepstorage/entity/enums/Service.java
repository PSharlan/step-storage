package by.itstep.stepstorage.entity.enums;

public enum Service {
    //Please keep an alphabetical order

    STEP_ACCOUNT,
    STEP_ACHIEVEMENT,
    STEP_AUTH,
    STEP_COWORKING,
    STEP_FEEDBACK,
    STEP_GATEWAY,
    STEP_HEROES,
    STEP_HOMEWORK,
    STEP_INTERVIEW,
    STEP_MAIL_SENDER,
    STEP_MENTORING,
    STEP_ROADMAP,
    STEP_STORAGE,
    STEP_TEST,
    STEP_UI_CMS,
    STEP_UI_WEB

}
